FROM node

# Создать директорию app
WORKDIR /app

# Установить зависимости приложения
# Используется символ подстановки для копирования как package.json, так и package-lock.json,
# работает с npm@5+
COPY package.json /app

RUN yarn install
# Используется при сборке кода в продакшене
# RUN npm install --only=production
COPY . /app
RUN yarn build
# Скопировать исходники приложения


EXPOSE 3000
CMD [ "yarn start" ]